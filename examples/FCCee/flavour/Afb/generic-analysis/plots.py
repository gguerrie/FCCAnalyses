import time
import sys
import os
import ROOT
import numpy as np
from array import array

#infile = sys.argv[1]
#outfile = sys.argv[2]

#print("Opening TFile: " + infile)
#f = ROOT.TFile.Open(infile)
f = ROOT.TFile.Open("/eos/home-g/gguerrie/public/outputs/FCCee/flavour/generic-analysis/test_new_evt/p8_ee_Zbb_ecm91.root")
myTree = f.Get("events")

hpxpy  = ROOT.TH2F( 'hpxpy', 'costheta_MC vs costheta_EVT', 200, -1, 1, 200, -1, 1 )
mc  = ROOT.TH1F( 'mc', 'costheta_MC', 50, -1, 1)
evt  = ROOT.TH1F( 'evt', 'costheta_EVT', 50, -1, 1)
mc_e  = ROOT.TH1F( 'mc_e', 'MC_e', 100, 0, 100)

EVT_thrust_costheta = 0
myTree.Branch("EVT_thrust_costheta", EVT_thrust_costheta, "EVT_thrust_costheta/F")

MC_pz = np.zeros(25)
myTree.Branch("MC_pz", MC_pz, "MC_pz/F");

MC_p = np.zeros(25)
myTree.Branch("MC_p", MC_p, "MC_p/F");

MC_e = np.zeros(25)
myTree.Branch("MC_e", MC_e, "MC_e/F");

MC_pdg = np.zeros(25)
myTree.Branch("MC_pdg", MC_pdg, "MC_pdg/F");

MC_status = np.zeros(25)
myTree.Branch("MC_status", MC_status, "MC_status/F");


for entry in range(myTree.GetEntriesFast()) :
#for entry in range(3000) :
  if entry % 10000 == 0 :
    print(str(entry)+"/"+str(myTree.GetEntriesFast()))
#  print("Getting entry")
  myTree.GetEntry(entry)
  evt.Fill(myTree.EVT_thrust_costheta)
#  print(myTree.EVT_thrust_costheta)
#  print("entering 2nd loop")
#  print(len(myTree.MC_pz))
  for num in range(len(myTree.MC_pz)) :
#    print(int(num))
    if (myTree.MC_pdg[int(num)]==5 and myTree.MC_status[int(num)]==23) :
#      print("Found it!")
      if (myTree.MC_py[int(num)]>=0) :
        costheta_mc = myTree.MC_pz[int(num)] / myTree.MC_p[int(num)]
      else:
        costheta_mc = -myTree.MC_pz[int(num)] / myTree.MC_p[int(num)]
        
      hpxpy.Fill(myTree.EVT_thrust_costheta, costheta_mc)
      mc.Fill(costheta_mc)
      mc_e.Fill(myTree.MC_e[int(num)])
      break
      
print("Drawing histo")

c1 = ROOT.TCanvas("c1")
c2 = ROOT.TCanvas("c2")
c3 = ROOT.TCanvas("c3")
c4 = ROOT.TCanvas("c4")

c1.cd()
hpxpy.Draw("COLZ")
hpxpy.GetXaxis().SetTitle("EVT_cos#theta_{thrust}");
hpxpy.GetYaxis().SetTitle("MC_cos#theta_{thrust}");
ROOT.gStyle.SetOptStat(0)
c1.SaveAs("examples/FCCee/flavour/Afb/plots/correlation.png")

c2.cd()
mc.Draw()
ROOT.gStyle.SetOptStat(0)
c2.SaveAs("examples/FCCee/flavour/Afb/plots/MC_costheta.png")

c3.cd()
evt.Draw()
ROOT.gStyle.SetOptStat(0)
c3.SaveAs("examples/FCCee/flavour/Afb/plots/EVT_costheta.png")

#c4.cd()
#mc_e.Draw()
#ROOT.gStyle.SetOptStat(0)
#c4.SaveAs("examples/FCCee/flavour/Afb/plots/mc_e_new.png")
    




